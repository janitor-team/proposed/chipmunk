chipmunk (7.0.3-5) unstable; urgency=medium

  * Build-depend on libgl-dev only, instead of libgl1-mesa-dev.
  * Standards-Version 4.6.2, no change required.
  * Record that spelling-fixes.patch has been forwarded.

 -- Stephen Kitt <skitt@debian.org>  Thu, 16 Feb 2023 14:54:09 +0100

chipmunk (7.0.3-4) unstable; urgency=medium

  * Update debian/watch.
  * Fix the spelling of “positive” (flagged by lintian).
  * Standards-Version 4.6.0, no change required.
  * Clean up the list of uploaders. Thanks to all the previous
    maintainers!

 -- Stephen Kitt <skitt@debian.org>  Mon, 13 Sep 2021 18:13:52 +0200

chipmunk (7.0.3-3) unstable; urgency=medium

  * Stop including sys/sysctl.h. Closes: #967981.
  * Drop the libchipmunk-dbg migration, it was never uploaded to the
    archives.
  * Switch to debhelper compatibility level 13.
  * Stop suggesting chipmunk-dev, it’s a transitional package.

 -- Stephen Kitt <skitt@debian.org>  Sat, 17 Oct 2020 11:15:31 +0200

chipmunk (7.0.3-2) unstable; urgency=medium

  * Only tweak examples on arch builds, the files are only present then.
    Thanks to Andreas Beckmann for spotting the bug and providing the
    fix. Closes: #958536.

 -- Stephen Kitt <skitt@debian.org>  Thu, 23 Apr 2020 18:12:26 +0200

chipmunk (7.0.3-1) experimental; urgency=low

  [ Andrew Kelley ]
  * add symbols file
  * add examples
  * enable multi arch
  * use GNUInstallDirs in cmake
  * simplify rules file
  * install upstream changelog
  * update copyright format
  * build with git-buildpackage and pristine-tar
  * update to new upstream on GitHub
  * install documentation

  [ Evgeni Golov ]
  * Correct Vcs-* URLs to point to anonscm.debian.org

  [ Simon McVittie ]
  * Correct Vcs-* URLs (again) to point to salsa.debian.org

  [ Stephen Kitt ]
  * New upstream release. Closes: #956794.
  * Add myself to “Uploaders”.
  * Switch to debhelper compatibility level 12.
  * Fix example installation: skip .gitignore, disable compression, fix
    permissions.
  * Configure gbp to use pristine-tar by default.
  * Specify Priority “optional” instead of “extra”.
  * Drop obsolete -dbg package.
  * Ship the development files in libchipmunk-dev rather than chipmunk-
    dev; the latter is now a transitional package.
  * Add a “Build-Depends-Package” entry to the symbols file.
  * Set “Rules-Requires-Root: no”.
  * Enable all hardening options.
  * Drop the unused dh-buildinfo build-dependency.
  * Standards-Version 4.5.0, no further change required.

 -- Stephen Kitt <skitt@debian.org>  Mon, 20 Apr 2020 18:14:56 +0200

chipmunk (6.1.5-1) unstable; urgency=low

  [ Barry deFreese ]
  * Add myself to uploaders.
  * New upstream release. (Closes: #679199).
  * Bump Debian SONAME to 0d3 for shared lib.
    + Upstream feels it shouldn't be used as a shared lib.
  * Add proper Vcs tags to debian/control.
  * Update Homepage again.
  * Drop build-dep on quilt since this is a source format 3.0 package.
  * Bump debhelper build-dep and compat to 9.
    + Change to dh style rules.
  * Add hardening flags.
  * Bump Standards Version to 3.9.4.

 -- Barry deFreese <bdefreese@debian.org>  Wed, 12 Jun 2013 05:21:14 -0400

chipmunk (6.1.1-1) UNRELEASED; urgency=low

  * Team upload.
  * New upstream release (Closes: #679199)
  * Add a watch file
  * Switch to new upstream website

 -- Paul Wise <pabs@debian.org>  Thu, 27 Sep 2012 11:34:47 +0800

chipmunk (5.3.4-1) unstable; urgency=low

  * Initial release. Closes: #623274

 -- Miriam Ruiz <little_miry@yahoo.es>  Mon, 25 Apr 2011 00:57:19 +0200
